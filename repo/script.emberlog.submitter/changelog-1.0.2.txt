[B][COLOR=FF56ADDA]1.0.2 - 28Jan17[/COLOR][/B]
• Fix bug that caused the addon to be unable to read device hardware information 

[B][COLOR=FF56ADDA]1.0.1 - 26Dec16[/COLOR][/B]
• Fix bug stopping starndard EmbER devices using the addon at all

[B][COLOR=FF56ADDA]1.0.0 - 17May16[/COLOR][/B]
• initial Release
