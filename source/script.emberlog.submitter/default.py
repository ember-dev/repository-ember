#!/usr/bin/env python
#
###################################################################################################
#
#                                ** EmbER Log Uploader for Kodi **                                
#
#   Adapted by:		Josh.5 (jsunnex@gmail.com)
#   Date:		16 May, 2016
# 
#
####################################################################################################


import xbmc, xbmcgui, xbmcaddon
import sys, urllib2, os, subprocess, base64

# Plugin Info
ID               = 'script.emberlog.submitter'
ADDON            = xbmcaddon.Addon( ID )
ADDON_ID         = ADDON.getAddonInfo('id')
ADDON_NAME       = ADDON.getAddonInfo('name')
ADDON_ICON       = ADDON.getAddonInfo('icon')
ADDON_VERSION    = ADDON.getAddonInfo('version')
ADDON_DATA       = xbmc.translatePath( "special://profile/addon_data/%s/" % ID )
ADDON_DIR        = ADDON.getAddonInfo( "path" )
KODI_HOME        = xbmc.translatePath("special://home")
PATCH_DIR        = os.path.join(ADDON_DATA, 'PATCHES')

# Plugin Settings
URL              = "http://ember-dev.com/FIRMWARE/upload_kodi_logs.php"
CONTACT          = "http://ember-dev.com/contact"

### Add lib folder to py path ###
BASE_RESOURCE_PATH = xbmc.translatePath( os.path.join( ADDON_DIR, 'resources', 'lib' ) )
sys.path.append (BASE_RESOURCE_PATH)


### FUNCTIONS ###
def upload(ul_file, email):
    from poster.encode import multipart_encode
    from poster.streaminghttp import register_openers
    CWD = os.getcwd()
    register_openers()
    emberid = get_hardware_info() # <-- needed if we expand to system logs
    if emberid:
        datagen, headers = multipart_encode({"fileToUpload": open(ul_file, "rb"), "name": ul_file, "email": email, "emberid": emberid})
        request = urllib2.Request(URL, datagen, headers)
        return urllib2.urlopen(request).read().split('_$', 1)[0]
    else: return False;

def get_hardware_info():
    if xbmc.getCondVisibility('System.HasAddon(script.system.settings)') and xbmcaddon.Addon(id = "script.system.settings").getSetting("OEM_DIST") in ["ARNU BOX", "MBOX"]: return "ARNU";
    elif xbmc.getCondVisibility('System.HasAddon(script.system.settings)'): Utype = base64.b64decode('YXV0b2h3c24='); info = 'python '+xbmc.translatePath(os.path.join(xbmcaddon.Addon( "script.system.settings" ).getAddonInfo( "path" ), 'resources', 'lib', 'systemsettings', base64.b64decode('cnNzcHlvZW0ucHlj'))); output = subprocess.check_output(info+' '+Utype, shell=True).rstrip('\n'); return output;
    else: return False;

def submit_log_dialog():
    log_sources = [
      ['kodi.log - This instance of Kodi', 'kodi.log', os.path.join(xbmc.translatePath('special://home'), 'temp', 'kodi.log')],
      ['kodi.old.log - The last instance of Kodi', 'kodi.old.log', os.path.join(xbmc.translatePath('special://home'), 'temp', 'kodi.old.log')]
    ]
    ls_description = []
    for x in log_sources: 
        ls_description.append(x[0])
    choice = xbmcgui.Dialog().select('Which Log file would you like to upload', ls_description)
    if not choice == -1: logfile = log_sources[choice][1]; logfile_src = log_sources[choice][2];
    else: return
    if os.path.isfile(logfile_src):
        email = xbmcgui.Dialog().input('Enter Your Email Address (Required)', type=xbmcgui.INPUT_ALPHANUM)
        if not email in ['', ' ']: response = upload(logfile_src, email);
        else: response = 'INVALIDEMAIL';
        if response in ['INVALIDEMAIL']:
            xbmcgui.Dialog().ok(ADDON_NAME,'Invalid email address!  Please enter a valid email address.\nIf you continue to experience issues, contact us at [B]%s[/B]' % CONTACT)
        if response in ['DALYLIMITEXCEEDED']:
            xbmcgui.Dialog().ok(ADDON_NAME,'Unable to upload file. You have reached the maximum number of log submissions.\nTo proceed, contact us at [B]%s[/B]' % CONTACT)
        if response in ['NOTLOGFILE']:
            message = "XBMC.Notification(%s, %s %s, 5000, %s)" % (ADDON_NAME, '[B]ERROR![/B]', 'Server rejected file. Please try again...', ADDON_ICON); xbmc.executebuiltin(message);
        if response in ['SIZELIMITEXCEEDED']:
            question = xbmcgui.Dialog().yesno(ADDON_NAME,'Unable to upload file. Your Log file appears to be too large.\nWould you like to transfer the file to an SD Card or USB device instead?')
            if question:
                while True:
                    location = xbmcgui.Dialog().browse(3, 'Where you would like to save you log file?', 'files', '', False, False, ''); 
                    print "location:"
                    print location+location
                    if os.path.isdir(location):
                        logfile_dest = os.path.join(location, logfile); import shutil; shutil.copy(logfile_src, logfile_dest);
                        xbmcgui.Dialog().ok(ADDON_NAME,'Log file copied to:\n[B]%s[/B]' % logfile_dest)
                        break;
                    elif location in ['', ' ']:
                        question = xbmcgui.Dialog().yesno(ADDON_NAME,'Would you like to cancel the file transfer?');
                        if question: break;
                    else: xbmcgui.Dialog().ok(ADDON_NAME,'Unable to copy to this directory. Please try again');
            else: pass;
        if response in ['ERROR', '', ' ']:
            message = "XBMC.Notification(%s, %s %s, 5000, %s)" % (ADDON_NAME, '[B]ERROR![/B]', 'Server rejected file. There is nothing I can do...', ADDON_ICON); xbmc.executebuiltin(builtin);
            xbmcgui.Dialog().ok(ADDON_NAME+' - CRITICAL ERROR!','Server rejected file.\nYou can try to upload the file again, but if this keeps happening, contact us at [B]%s[/B]' % CONTACT)
        if response in ['SUCCESS']:
            xbmcgui.Dialog().ok(ADDON_NAME,'File successfully submitted.\nBe sure you have notified us of this submission or nothing will be done about it.\nTo proceed, contact us at [B]%s[/B]' % CONTACT)


if __name__ == '__main__':
    submit_log_dialog()
