Kodi Repository - EmbER-Dev Add-ons for Kodi
================

Official EmbER-Dev Add-on Repository

## Introduction
This repository contains a number of add-ons for Kodi that are either proven to work on, or are purpose built for the Embedded Entertainment ROM.

## Installation
1. Download the [repository zip](https://gitlab.com/ember-dev/repository-ember/raw/master/downloads/repository.ember.zip) 
and install it in Kodi.
2. Read the following [guide](http://kodi.wiki/view/HOW-TO:Install_add-ons_from_zip_files) 
on the Kodi wiki to learn how to install addons from a repository.

## F.A.Q.
### When I install the repository from ZIP file, Kodi complains that it "does not have the correct structure".
This has typically meant that you have downloaded this github repository as a ZIP file (by clicking on the
'Download as zip' button) in the downloads page. Instead, you need to download the repository from the link
in the 'Download Packages' section of the downloads page.

## Contribution
If you want to contribute to this repository you can reach us at [EmbER-Dev.com](http://ember-dev.com/about/#double-block).

